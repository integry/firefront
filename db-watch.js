var _ = require('lodash');
var firebase = require('firebase');
var data = require('./storage/memory.js');
const p = require('./lib/path.js');

var id = process.argv[2];
var config = null;
var paths = {};
var ref = null;

process.on('message', function(msg)
{
	if (msg.config)
	{
		config = msg.config;
		eval('config = ' + config);
		firebase.initializeApp(config);
		ref = firebase.database().ref();
	}

	if (msg.path_added)
	{
		setTimeout(function()
		{
			watchPath(msg.path_added);
		}, 1000);
	}

	if (msg.path_removed)
	{
		unwatchPath(msg.path_added);
	}
});

function watchPath(path)
{
	paths[path] = firebase.database().ref(path);
	paths[path].on('child_added', function(snap)
	{
		registerValue(path, snap, 'child_added');
	});
	paths[path].on('child_changed', function(snap)
	{
		registerValue(path, snap, 'child_changed');
	});
}

function unwatchPath(path)
{
	delete paths[msg.path_added];
}

var valueTimeout = {};

function registerValue(path, snap, type)
{
	if ((type == 'child_added') || (type == 'child_changed'))
	{
		data.set(path, snap.key, snap.val());

		// send a "value" event after a stream of "child_added" events has finished
		clearTimeout(valueTimeout[path]);
		valueTimeout[path] = setTimeout(function()
		{
			console.log('MUST REWRITE THIS');
			process.send({t: 'value', p: path, v: data.get(path)});

			var parts = p.split(path);
			while (parts.length)
			{
				parts.pop();
				var parent = p.glue(parts);
				process.send({t: 'value', p: parent, v: data.get(parent)});
			}
		}, 10);

		process.send({t: type, p: path, v: snap.val()});
		process.send({t: 'value', p: path + '/' + snap.key, v: snap.val()});
	}
	else if (type == 'child_removed')
	{
		data.remove(path, snap.key);

		// @todo: check if parent nodes are empty and remove them as well
	}
}
