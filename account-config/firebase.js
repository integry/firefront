/* accounts configured in Firebase */

const firebase = require('firebase');

var config = {
	apiKey: "AIzaSyA5Pn0LSnEUjdJWFof4r4-P_KoVqu9sjFo",
	authDomain: "firefrnt.firebaseapp.com",
	databaseURL: "https://firefrnt.firebaseio.com",
	projectId: "firefrnt",
	storageBucket: "firefrnt.appspot.com",
	messagingSenderId: "412631913025"
};

firebase.initializeApp(config);

var act = firebase.database().ref('accounts');

act.on('child_added', function(snap)
{
	var account = snap.val();
	account.id = snap.key;
	process.send(account);
	
	act.child(account.key).child('paths').on('child_added', function(snap)
	{
		process.send({type: 'config', path_added: snap.val()});
	});
});
