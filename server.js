/* Server */

const _ = require('lodash');
const { fork } = require('child_process');
const WebSocket = require('ws');
const p = require('./lib/path.js');
const fs = require('fs');

require('./storage/rejson.js');

var storage = {};

var accounts = {};

// get the configuration source for accounts/databases
var args = process.argv.slice(2);
var accountConfig = args[0] || 'test';

// start database instances
var accountsWatch = fork('./account-config/' + accountConfig + '.js');
accountsWatch.on('message', function(msg)
{
	if (msg.type == 'account')
	{
		addAccount(msg.account);
	}
	else if (msg.type == 'config')
	{
		setAccountConfig(msg);
	}
});

// load filter instances
var filterInstances = {};
fs.readdirSync('filters').forEach(file => {
	if (file.substr(-3, 3) == '.js')
	{
		filterInstances[file.substr(0, file.length - 3)] = require('./filters/' + file);
	}
});

function addAccount(account)
{
	console.log('New account', account);

	accounts[account.id] = account;

	account.get = function(root, path, callback)
	{
		var realPath = p.realPath(path);
		var filters = p.filters(path);

		var storage = account.getStorage(realPath);

		if (filters.length)
		{
			var supportedFilters = storage.getCapabilities();
			var remaining = [];

			console.log(filters);

			_.each (filters, function(filter)
			{
				if (supportedFilters[filter[0]])
				{
					realPath += '/#' + filter.join('#');
				}
				else
				{
					remaining.push(filter);
				}
			});

			filters = remaining;
		}

		storage.get(root, realPath, function(res)
		{
			_.each(filters, function(filter)
			{
				if (filterInstances[filter[0]])
				{
					res = filterInstances[filter[0]].filter(res, filter.slice(1));
				}
			});

			callback(res);
		});
	};

	account.set = function(root, path, value, callback)
	{
		var storage = account.getStorage(path);
		storage.set(root, p.realPath(path), value, callback);
	};

	account.getStorage = function(path)
	{
		var type = this.engine || 'memory';
		if (!storage[type])
		{
			storage[type] = require('./storage/' + type + '.js');
		}

		return storage[type];
	};

	if (account.firebase)
	{
		var dbWatch = fork('./db-watch.js', [account.id]);
	}
	else
	{
		var dbWatch = fork('./direct-watch.js', [account.id]);
	}

	account.watcher = dbWatch;
	account.connections = [];

	// @todo: cache, optimize...
	account.getWatchedPaths = function(path, type)
	{
		var paths = [];
		_.each(this.connections, function(connection)
		{
			paths = paths.concat(connection.getWatchedPaths(path, type));
		});

		return paths;
	}

	dbWatch.send({config: accounts[account.id].config});

	// notification on data changes
	dbWatch.on('message', function(data)
	{
		registerValue(account, data.path, data.type, data.value, function(data)
		{
			if (account.getWatchedPaths(data.path, data.type).length)
			{
				_.each(account.connections, function(conn)
				{
					if (conn.getWatchedPaths(data.path, data.type).length)
					{
						conn.notify(data.type, data);
					}
				});
			}
		});
	});

	dbWatch.on('error', function(err)
	{
		console.log(error, err);
	});

	return account;
};

var account = {
	id: '.server',
	engine: 'server',
	config: {}
};

var acc = addAccount(account);
acc.getStorage().inject(accounts);

// dummy uuid generator
var sequentialUuid = function(account, path)
{
	return '#########' + Date.now() + "-" + Math.round(Math.random() * 100000);
};

var valueTimeout = {};
function registerValue(account, path, type, value, notifyCallback)
{
	if ('child_added' == type)
	{
		//~ console.log('FORWARDING', account, path + '/' + sequentialUuid(account, path));
		registerValue(account, path + '/' + sequentialUuid(account, path), 'value', value, notifyCallback);
		return;
	}

	account.get(account.id, path, function(current)
	{
		if (JSON.stringify(value) == JSON.stringify(current))
		{
			return;
		}

		account.set(account.id, path, value, function(value)
		{
			notifyCallback({path: path, value: value, type: 'value'});

			var parent = p.parent(path);

			// send child_added event immediately to the immediate parent
			if (!valueExists(current))
			{
				//~ console.log('ADDED', parent, value, path);
				notifyCallback({path: parent, type: 'child_added', value: value, child: p.last(path)});
			}

			// send value/child_changed events after a stream of children events has finished
			do
			{
				// child_added/changed

				// get all variations of the watched path (include filters)
				var watchedPaths = account.getWatchedPaths(parent, 'value');
				console.log('Watched paths for', parent, ' =>>', watchedPaths);

				_.each(watchedPaths, function(path)
				{
					clearTimeout(valueTimeout[path]);
					valueTimeout[path] = setTimeout((function(path) { return function()
					{
						console.log('PARENT', path);

						account.get(account.id, path, function(parentValue)
						{
							notifyCallback({path: path, type: 'value', value: parentValue});
							// todo: child_changed
						});
					}})(path), 100);
				});

				parent = p.parent(parent);
			}
			while (parent);
		});
	});

	/*
	if ((type == 'child_added') || (type == 'child_changed'))
	{
		data.set(path, snap.key, snap.val());

		// send a "value" event after a stream of "child_added" events has finished
		clearTimeout(valueTimeout[path]);
		valueTimeout[path] = setTimeout(function()
		{
			process.send({t: 'value', p: path, v: data.get(path)});

			var parts = p.split(path);
			while (parts.length)
			{
				parts.pop();
				var parent = p.glue(parts);
				process.send({t: 'value', p: parent, v: data.get(parent)});
			}
		}, 10);

		process.send({t: type, p: path, v: snap.val()});
		process.send({t: 'value', p: path + '/' + snap.key, v: snap.val()});
	}
	else if (type == 'child_removed')
	{
		data.remove(path, snap.key);

		// @todo: check if parent nodes are empty and remove them as well
	}
	*/
}

function valueExists(value)
{
	return (value !== null) && (value !== undefined);
}

function setAccountConfig(msg)
{
	var account = accounts[msg.account];
	account.watcher.send(msg);
};

// @todo: configurable port and other settings
const wss = new WebSocket.Server({ port: 2018 });

// @todo: clean up dead connections
wss.on('connection', function connection(ws, data) {

	console.log('>>>>>>>>>>>>>>>> ', data.url);
	var db = data.url.substr(1, data.url.length);
	var acct = accounts[db];
	if (!acct)
	{
		console.log('Invalid database: ' + db);
		ws.send(JSON.stringify({error: 'Invalid database: ' + db}));
		ws.close();
		return;
	}

	acct.connections.push(new (function()
	{
		/**
			A tree representing the watched paths. For instance the following paths are watched:

			smth/sub/a
			smth/test

			the tree would look like this:

			smth
			  sub
			    a
			      .watched: true
			  test
			    .watched: true
		*/
		var watched = {};

		var self = this;

		var eventTypes = {
			'+': 'child_added',
			'-': 'child_removed',
			'~': 'child_changed',
			'=': 'value'
		};

		var findWatched = function(path, w)
		{
			var ret = [];

			// include filter paths only
			_.each(w, function(sub, key)
			{
				if (key.substr(0, 1) == '#')
				{
					ret = ret.concat(findWatched(path + '/' + key, sub));
				}
			});

			if (w && w['.watched'])
			{
				ret.push(path);
			}

			return ret;
		};

		this.getWatchedPaths = function(path, type)
		{
			var w = _.get(watched[type], p.split(path));

			return findWatched(path, w);
		};

		this.notify = function(type, data)
		{
			var path = data.path;
			var value = data.value;

			console.log('________', type, path, value);
			var w = _.get(watched[type], p.split(path));

			if (!w)
			{
				//~ console.log('not watching', type, path, JSON.stringify(watched[type]));
				return;
			}

			if (w['.watched'])
			{
				//~ console.log('NOTIFY', type, path, value, watched);

				// @todo: prevent repeated notifications

				try
				{
					var toSend = [type, path, value];
					if (data.child)
					{
						toSend.push(data.child);
					}

					ws.send(JSON.stringify(toSend));
				}
				catch (e)
				{
					console.log('Disconnecting client');
					var idx = acct.connections.indexOf(self);
					if (idx > -1)
					{
						acct.connections.splice(idx, 1);
					}
				}
			}

			_.each(w, function(foo, sub)
			{
				if ('.watched' != sub)
				{
					var subValue = _.get(value, sub);
					if (valueExists(subValue))
					{
						self.notify(type, {path: path + '/' + sub, value: subValue});
					}
				}
			});
		};

		var watch = function(path, type)
		{
			if (!watched[type])
			{
				watched[type] = {};
			}

			var pt = path.split('/');
			var curr = _.get(watched[type], pt) || {};
			curr['.watched'] = true;

			_.set(watched[type], pt, curr);

			acct.get(acct.id, path, function(current)
			{
				if (!_.isUndefined(current) && (current !== null))
				{
					//~ console.log('@@@@@ Notifying exisisting value', path, current);
					self.notify(type, {path: path, value: current});
				}
			});
		};

		var unwatch = function(path, type)
		{
			if (watched[type])
			{
				delete watched[type][path];
			}
		};

		var write = function(msg, type)
		{
			msg = JSON.parse(msg);
			acct.watcher.send([type, msg.p, msg.v]);
		};

		var command = function(cmd)
		{
			if ('restart' == cmd)
			{
				console.log('KTHXBYE');
				process.exit();
			}
		};

		var actionTypes = {
			'+': watch,
			'-': unwatch,
			'w': write,
			'c': command
		};

		ws.on('message', function incoming(message)
		{
			console.log(message);
			var actionType = message.substr(0, 1);
			var a = actionTypes[actionType];
			if (command == a)
			{
				command(message.substr(1, message.length));
				ws.send('1');
			}
			else
			{
				var eventType = message.substr(1, 1);
				var path = message.substr(2, message.length);

				var e = eventTypes[eventType];
				if (a && e)
				{
					a(path, e);
					ws.send('1');
				}
			}

			//~ console.log('received: %s', message);
		});
	}));


});

//~ function heartbeat() {
  //~ this.isAlive = true;
//~ }

//~ wss.on('connection', function connection(ws) {
  //~ ws.isAlive = true;
  //~ ws.on('pong', heartbeat);
//~ });

//~ const interval = setInterval(function ping() {
  //~ wss.clients.forEach(function each(ws) {
    //~ if (ws.isAlive === false) return ws.terminate();

    //~ ws.isAlive = false;
    //~ ws.ping('', false, true);
  //~ });
//~ }, 30000);
