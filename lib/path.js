module.exports = {
	clean: function(path)
	{
		if (path)
		{
			while (path[0] == '/')
			{
				path = path.substr(1, path.length);
			}

			while (path[path.length] == '/')
			{
				path = path.substr(0, path.length - 1);
			}
		}

		return path || '';
	},

	realPath: function(path, returnFilters)
	{
		var parts = this.split(path);
		var filters = [];
		for (var k = parts.length - 1; k >= 0; k--)
		{
			if (parts[k].substr(0, 1) == '#')
			{
				filters.push(parts[k].substr(1, parts[k].length).split('#'));
				parts.pop();
			}
			else
			{
				break;
			}
		}

		return returnFilters ? filters : this.glue(parts);
	},

	filters: function(path)
	{
		return this.realPath(path, true);
	},

	split: function(path, subpath)
	{
		var fullPath = subpath ? path + '/' + subpath : path;
		var p = this.clean(fullPath);
		return p.split('/') || [];
	},

	glue: function(parts)
	{
		return parts.join('/') || '';
	},

	last: function(path, subpath)
	{
		var p = this.split(path, subpath);
		return p.pop();
	},

	parent: function(path, subpath)
	{
		var p = this.split(path, subpath);
		p.pop();

		return this.glue(p);
	},
};
