import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

declare var firefront: any;

@Component({
  selector: 'app-database',
  templateUrl: './database.component.html',
  styleUrls: ['./database.component.less']
})
export class DatabaseComponent implements OnInit {

  database: string;
  path: '';

  @Input() rootNode: TreeNodeComponent;

  constructor(private route: ActivatedRoute) {

	  this.route.params.subscribe( params => this.database = params.database );

  }

  ngOnInit() {

	firefront.initializeApp(this.database);
	this.dbInstance = firefront.database(this.database);
  }

}
