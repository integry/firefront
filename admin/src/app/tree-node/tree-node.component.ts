import { Component, OnInit, Input } from '@angular/core';

declare var firefront: any;

@Component({
  selector: 'app-tree-node',
  templateUrl: './tree-node.component.html',
  styleUrls: ['./tree-node.component.less']
})
export class TreeNodeComponent implements OnInit {

  @Input() path: string;
  @Input() database: {};
  @Input() parent: TreeNodeComponent;

  keys: string[];
  expanded: {};

  constructor() { }

  ngOnInit() {

	let db = this.getDatabase();

	let root = db.ref(this.path);

	root.child('ahh').child(Math.random().toString(36).substring(7)).set('test');

	root.child('a').once('value', function(snap) { console.log(snap.val()); });

	this.tree = {};

	root.keys().on('value', snap =>
	{
		this.keys = snap.val();
	});

	this.expanded = {};
  }

  expand(key string)
  {
	this.expanded[key] = true;
	console.log(key);
  }

  getDatabase()
  {
	return this.parent.dbInstance || this.parent.getDatabase();
  }


}
