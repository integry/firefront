import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DatabaseComponent } from './database/database.component';
import { DatabaseListComponent } from './database-list/database-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TreeNodeComponent } from './tree-node/tree-node.component';

const appRoutes: Routes = [
  { path: 'db/:database', component: DatabaseComponent },
  { path: 'db',      component: DatabaseListComponent },
  { path: '',
    redirectTo: '/db',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DatabaseComponent,
    DatabaseListComponent,
    PageNotFoundComponent,
    TreeNodeComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
