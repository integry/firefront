import { Component, OnInit } from '@angular/core';
declare var firefront: any;

@Component({
  selector: 'app-database-list',
  templateUrl: './database-list.component.html',
  styleUrls: ['./database-list.component.less']
})
export class DatabaseListComponent implements OnInit {

  public databases: string[];

  constructor() { }

  vm = this;

  ngOnInit() {

	firefront.initializeApp('.server');
  	let db = firefront.database('.server');

	db.ref('databases').on('value', (snap) =>
	{
		this.databases = snap.val();
	});


  }

}
