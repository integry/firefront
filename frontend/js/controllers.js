angular.module('firefront.controllers', [])

.controller('ListCtrl', function ($scope, $timeout)
{
	var vm = this;

	//~ var config = {
		//~ databaseURL: "http://localhost/.server",
	//~ };
	//~ firebase.initializeApp(config);
	firefront.initializeApp('.server');
	var db = firefront.database('.server');

	db.ref('databases').on('value', function(snap)
	{
		$timeout(function()
		{
			vm.databases = snap.val();
		});
	});

})

.controller('DatabaseCtrl', function ($scope, $stateParams)
{
	var vm = this;

	firefront.initializeApp($stateParams.database);
	var db = firefront.database($stateParams.database);

	vm.tree = {};

	vm.path = $stateParams.path || '';

	var root = db.ref(vm.path);

	//~ root.child('a').set('test');

	root.child('a').once('value', function(snap) { console.log(snap.val()); });

	root.keys().on('value', function(snap)
	{
		var keys = snap.val();
		console.log(keys);
		_.each(keys, function(key)
		{
			vm.tree[key] = {};
		});
	});

	vm.loadJson = function(json)
	{
		db.ref(vm.path).set(json);
	};

})

;
