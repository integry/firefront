angular.module('firefront', ['firefront.controllers', 'ui.bootstrap', 'ui.router', 'ngAnimate'])

.config(function($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider, $provide) {

  //~ $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('!');

  $stateProvider

  .state('list', {
  	url: '/',
  	templateUrl: 'templates/list.html',
  	controller: 'ListCtrl',
	controllerAs: 'list'
  })

  .state('database.path', {
  	url: '/db/:database/:path',
  	templateUrl: 'templates/database.html',
  	controller: 'DatabaseCtrl',
	controllerAs: 'db'
  })

  .state('database', {
  	url: '/db/:database',
  	templateUrl: 'templates/database.html',
  	controller: 'DatabaseCtrl',
	controllerAs: 'db'
  })

  $urlRouterProvider.otherwise('/');
});
