var _ = require('lodash');

var id = process.argv[2];
var config = null;
var paths = {};
var ref = null;

process.on('message', function(msg)
{
	if (!msg.config)
	{
		console.log(msg);
		process.send({type: msg[0], path: msg[1], value: msg[2]});
		console.log('value update received', msg);
	}
	else
	{
		console.log('config received', msg);
	}
});
