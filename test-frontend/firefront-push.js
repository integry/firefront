const _ = require('lodash');
const firefront = require('../firefront.js');

firefront.initializeApp('fx');

	firefront.database('fx').ref('test').on('value', function(snap)
	{
		console.log('receiving value', snap.val());
	});

	//~ firefront.database('fx').ref('mehh').on('value', function(snap)
	//~ {
		//~ console.log('receiving MEHH value', snap.val());
	//~ });

	setTimeout(function()
	{
		firefront.database('fx').ref('test').set('blah blah');
	}, 500);
