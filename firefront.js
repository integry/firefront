/* Client side library */

if (typeof window === 'undefined')
{
	var WebSocket = require('ws');
	var _ = require('lodash');

	process.on('uncaughtException', function (err) {
		console.log('uncaught exception', err);
	});
}

var ffServer = 'ws://localhost:2018/';

firefront = function()
{
	var connections = {};

	var sendQueue = {};

	var p = function(path)
	{
		path = path || '';
		while (path.length && (path[0] == '/'))
		{
			path = path.substr(1, path.length);
		}

		while (path.length && (path[path.length] == '/'))
		{
			path = path.substr(0, path.length - 1);
		}

		return path;
	};

	var eventTypes = {
		child_added: '+',
		child_removed: '-',
		child_changed: '~',
		value: '='
	};

	var connect = function(database)
	{
		console.log('connecting', database);
		sendQueue[database] = sendQueue[database] || [];

		var establishConnection = function(url, callback, attempt, watches)
		{
			// gradually increase the intervals between connection attempts in case of a longer downtime
			attempt = Math.min((attempt || 0) + 1, 50);

			var socket = new WebSocket(url);
			socket.onerror = function()
			{
				console.log('ERROR event');
				socket.close();
			}

			socket.onclose = function()
			{
				console.log('CLOOSE event');

				var watches = connections[database].getWatches();

				setTimeout(function()
				{
					establishConnection(url, callback, attempt, watches);
					console.log('Connection attempt ', attempt);
				}, attempt * 100);
			}

			callback(socket, watches);
		};

		establishConnection(ffServer + database, function(socket, existingWatches)
		{

		socket.onopen = function()
		{
			processQueue();
			console.log('OK, IT OPENED!');
		};

		var send = function(msg, callback)
		{
			console.log(msg);
			sendQueue[database].push([msg, callback]);
			processQueue();
		};

		var processQueue = function()
		{
			if (socket.readyState == socket.CONNECTING)
			{
				return;
			}

			if (socket.readyState != socket.OPEN)
			{
				console.log('socket not open');
				return;
			}

			while (sendQueue[database].length)
			{
				var cmd = sendQueue[database].shift();

				(function(cmd)
				{
					var cb = cmd[1];

					try
					{
						socket.send(cmd[0], function() {
							if (cb)
							{
								// @todo: timeout not needed?
								setTimeout(function()
								{
									cb();
								});
							}
						});
					}
					catch (e)
					{
						sendQueue[database].unshift(cmd);
						console.log('Caught a connection error');
						console.log(e);
					}
				})(cmd);
			}
		};

		connections[database] = new function(socket)
		{
			var c = this;
			var watches = {};
			var values = {};

			socket.addEventListener('open', function (event)
			{
				console.log('Resending watches');
			});

			this.getWatches = function()
			{
				return watches;
			};

			this.setWatches = function(w)
			{
				watches = w;
			};

			// @todo: queue messages, reconnect on error
			this.watch = function(path, type, callback)
			{
				path = p(path);

				if (!watches[path])
				{
					watches[path] = {};
				}

				wp = watches[path];

				// send a notification to the server that we're watching this space
				if (!wp[type])
				{
					wp[type] = [];
					send('+' + eventTypes[type] + path);
				}

				wp[type].push(callback);

				// do we have a locally stored value already?
				if (('value' == type) && (values[path]))
				{
					this.notify([path, type, values[path]], true);
				}
			};

			this.unwatch = function(path, type, callback)
			{
				path = p(path);

				wp = watches[path];
				if (!wp || !wp[type])
				{
					return;
				}

				if (!callback)
				{
					wp[type] = [];
				}
				else
				{
					var i;
					while ((i = wp[type].indexOf(callback)) > -1)
					{
						wp[type].splice(i, 1);
					}
				}

				// @todo: assign IDs to paths server side
				if (!wp[type].length)
				{
					send('-' + path);
				}
			};

			// set, update, remove
			this.send = function(path, type, value, callback)
			{
				var eventTypes = {
					'set': '=',
					'push': '+',
					'update': '>',
				};

				var val = 'w' + eventTypes[type] + JSON.stringify({p: path, v: value});

				send(val, function()
				{
					if (callback)
					{
						callback();
					}
				});
			};

			this.sendCommand = function(command, callback)
			{
				var val = 'c' + command;

				send(val, function()
				{
					if (callback)
					{
						callback();
					}
				});
			};

			// @todo: snapshot key
			this.notify = function(data, onlyLastInstance)
			{
				var type = data[0];
				var path = data[1];
				var value = data[2];
				var key = data[3] || '';

				//~ console.log('notifying', path, type, value, onlyLastInstance);

				path = p(path);
				values[path] = value;

				wp = watches[path];
				if (!wp || !wp[type])
				{
					this.unwatch(path, type);
					return;
				}

				var wt = wp[type];
				var from = onlyLastInstance ? wt.length - 1 : 0;

				var queue = [];
				for (var k = from; k < wt.length; k++)
				{
					queue.push(wt[k]);
				}

				for (var k = 0; k < queue.length; k++)
				{
					// @todo - create snapshot instance once? but prevent modification
					var snapshot = {
						val: function()
						{
							return JSON.parse(JSON.stringify(value));
						},

						key: key
					};

					queue[k](snapshot);
				}
			};

			return this;
		}(socket);

		if (existingWatches)
		{
			_.each(existingWatches, function(watches, path)
			{
				_.each(watches, function(callbacks, type)
				{
					_.each(callbacks, function(cb)
					{
						console.log('Rewatching', path, type);
						connections[database].watch(path, type, cb);
					});
				});
			});

			console.log(sendQueue);
			processQueue();
		}

		socket.addEventListener('connect', function (event) {
			console.log('OK, WERE CONNECDTED');
			console.log(sendQueue);
			processQueue();
		});

		//~ socket.onclose = function()
		//~ {
			//~ console.log('Crap, the server is gone!');
			//~ connect(database);
		//~ };

		// @todo: optimize data format
		socket.onmessage = function(event)
		{
			// @todo - success / error?
			if ('1' == event.data)
			{
				return;
			}

			try
			{
				var data = JSON.parse(event.data);
				//~ console.log(data);
				connections[database].notify(data);
			}
			catch (e) { console.log('Cannot parse', event.data, e); }

		};

		}); // << on connect
	};

	return {
		initializeApp: function(id)
		{
			connect(id);
		},

		database: function(id)
		{
			// @todo: use default id
			if (!id)
			{

			}

			var db = this;
			var c = connections[id];

			this.ref = function(path)
			{
				path = p(path);

				this.filters = [];

				this.on = function(type, callback)
				{
					c.watch(path, type, callback);
				};

				this.once = function(type, callback)
				{
					var self = this;
					var cb = function(val)
					{
						callback(val);
						self.off(type, cb);
					};
					cb.isOnce = true;

					c.watch(path, type, cb);
				};

				this.off = function(type, callback)
				{
					c.unwatch(path, type, callback);
				};

				this.child = function(subpath)
				{
					return new db.ref(path + '/' + (subpath || ''));
				};

				this.set = function(value, callback)
				{
					c.send(path, 'set', value, callback);
				};

				this.update = function(value, callback)
				{
					c.send(path, 'update', value, callback);
				};

				this.push = function(value, callback)
				{
					c.send(path, 'push', value, callback);
				};

				this.cmd = function(command, callback)
				{
					c.sendCommand(command, callback);
				};

				this.keys = function()
				{
					return new db.ref(path + '/#keys');
				};

				this.fields = function(fields)
				{
					return new db.ref(path).filter({fields: fields});
				};

				this.filter = function(filter)
				{
					return new db.ref(path + '/#filter#' + JSON.encode(filter));
				};

				this.shallow = function()
				{
					return new db.ref(path + '/#shallow');
				};

				this.ref = this.child;
			}

			return new this.ref('');
		},

		turnOff: function()
		{

		},

		turnOn: function()
		{

		},
	}

}();

if (typeof window === 'undefined')
{
	module.exports = firefront;
}
