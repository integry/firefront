const p = require('../lib/path.js');
const _ = require('lodash');
const fs = require('fs');
const Rejson = require('iorejson');
const Redis = require('ioredis');

var getReference = function(root, path)
{
	var parts = p.split(root, path);
	console.log(parts);

	// @todo: allow more than 2 parts per key for deeper objects, perhaps even autodetect by object size?
	var all = parts.join('.') || '';
	var key = _.filter(parts.splice(0, 2)).join('_');
	var path = parts.join('.') || '';

	return {key: key, path: path, all: all};
}

var connection = new Rejson();
connection.connect();

function getConnection()
{
	return connection;
}

var redis = new Redis();

var rootCreation = {};

function setValue(key, path, value, callback)
{
	path = path || '.';
	getConnection().set(key, path, value).then(function(value)
	{
		callback(value);
	}, function(err)
	{
		// looks like we have no roots - create the root node first
		var parts = path.split('.');
		var last = parts.pop();
		var root = parts.join('.');

		if (rootCreation[root])
		{
			setTimeout(function()
			{
				console.log('Pending creation', key, path);
				setValue(key, path, value, callback);
			});
			return;
		}

		rootCreation[root] = true;

		var newValue = {};
		newValue[last] = value;
		setValue(key, root, newValue, function(value)
		{
			callback(value);
			delete rootCreation[root];
		});
	});
};

module.exports = {
	getCapabilities: function()
	{
		return {
			keys: true
		};
	},

	set: function(root, path, value, callback)
	{
		var ref = getReference(root, path);
		setValue(ref.key, ref.path, value, function() { callback(value); });
	},

	update: function(root, path, value, callback)
	{

	},

	get: function(root, path, callback)
	{
		console.log('@@@@@', path);
		var last = p.last(path);
		if ('#keys' == last)
		{
			return this.getKeys(root, p.parent(path), callback);
		}

		var ref = getReference(root, path);

		var self = this;
		getConnection().get(ref.key, ref.path || '.').then(function(v) { callback(v); }, function(e)
		{
			callback(null);
			//~ console.log(e);
			//~ setTimeout(function()
			//~ {
				//~ self.get(root, path, callback);
			//~ });
		});
	},

	getKeys: function(root, path, callback)
	{
		var ref = getReference(root, path);

		if (path)
		{
			getConnection().objkeys(ref.key, ref.path || '.').then(function(v) { callback(v); });
		}

		// root keys
		else
		{
			redis.keys(ref.key + '_*').then(function(v)
			{
				var out = v.map(s => s.substr(ref.key.length + 1)).sort();
				callback(out);
			});
		}
	},

	remove: function(root, path)
	{
		//~ _.unset(data, getReference(root, path), value);
		//~ var parts = p.split(root, path);
		//~ var key = parts.pop();
		//~ var last = getReference(parts);

		//~ delete last[key];
	},

	isEmpty: function(root, path, callback)
	{
		//~ callback(_.has(data, getReference(root, path)));
	}
};
