const p = require('../lib/path.js');
const _ = require('lodash');
const fs = require('fs');

/**
 *  "Storage engine" for managing the database server and executing custom commands
 *
 *  Data structure:
 *
 * 		databases
 * 			db1		-> write DB config to create a new DB
 * 			db2		-> drop the node to delete a database
 * 			otherDB
 *
 * 		commands
 * 			stop	-> shut down the server
 *
 *
 */

var accounts = {};

module.exports = {
	getCapabilities: function()
	{
		return { };
	},

	set: function(root, path, value, callback)
	{
		var parts = p.split(path);
		var root = parts.shift();
		if ('commands' == root)
		{
			var cmd = parts.shift();

			if ('stop' == cmd)
			{
				console.log("---=== I'm tired. Will grab a sandwich. ===---");
				process.exit();
			}
		}
		else if ('database' == root)
		{
			var dbName = parts.shift();

			if (dbName)
			{

			}
		}
	},

	update: function(root, path, value, callback)
	{

	},

	get: function(root, path, callback)
	{
		console.log('---', root, path, path.length, _.isString(path));

		var self = this;

		if ('' == path)
		{
			console.log('ROOOOOOT');
			callback({databases: {}, commands: {}});
		}
		else if (path == 'databases')
		{
			var ret = {};
			_.each(accounts, function(acc)
			{
				ret[acc.id] = { config: acc.config };
			});
			console.log(ret);
			callback(ret);
		}
		else if (path == 'commands')
		{
			var ret = {
				stop: 0
			};

			callback(ret);
		}
		else
		{
			callback(null);
		}
	},

	inject: function(accts)
	{
		accounts = accts;
	},

	remove: function(root, path)
	{
		//~ _.unset(data, getReference(root, path), value);
		//~ var parts = p.split(root, path);
		//~ var key = parts.pop();
		//~ var last = getReference(parts);

		//~ delete last[key];
	},

	isEmpty: function(root, path, callback)
	{
		//~ callback(_.has(data, getReference(root, path)));
	}
};
