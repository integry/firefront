const p = require('../lib/path.js');
const _ = require('lodash');
const fs = require('fs');

var data = {};

var getReference = function(root, path)
{
	var parts = p.split(root, path);
	return _.filter(parts);
}

var filePath = '/tmp/firefront';
var lastSaveTS = 0;

if (fs.existsSync(filePath))
{
	try
	{
		data = JSON.parse(fs.readFileSync(filePath) + '');
	}
	catch (e)
	{
		data = {};
	}
}

module.exports = {
	save: function()
	{
		fs.writeFile(filePath, JSON.stringify(data), function() { });
	},

	set: function(root, path, value, callback)
	{
		_.set(data, getReference(root, path), value);
		this.save();

		if (callback)
		{
			callback(value);
		}
	},

	get: function(root, path, callback)
	{
		var res = _.get(data, getReference(root, path));

		callback(res);
	},

	remove: function(root, path)
	{
		_.unset(data, getReference(root, path), value);
		var parts = p.split(root, path);
		var key = parts.pop();
		var last = getReference(parts);

		delete last[key];
		this.save();
	},

	isEmpty: function(root, path, callback)
	{
		callback(_.has(data, getReference(root, path)));
	}
};