var assert = require('assert');
const _ = require('lodash');
const firefront = require('../firefront.js');

firefront.initializeApp('.server');

var db = firefront.database('.server');

describe('Manage databases', function() {

	it('should return shallow response for root node', function(done)
	{
		db.ref().on('value', function(snap)
		{
			var val = snap.val();
			assert.equal('{}', JSON.stringify(val.databases));
			assert.equal('{}', JSON.stringify(val.commands));

			setTimeout(function()
			{
				done();
			}, 100);
		});
	});

	it('should list the available databases', function(done)
	{
		db.ref('databases').on('value', function(snap)
		{
			var val = snap.val();
			assert.equal(_.keys(val).length > 0, true);
			assert.equal(_.keys(val).indexOf('.server') > -1, true);

			setTimeout(function()
			{
				done();
			}, 100);
		});
	});

	//~ it('should shut down the server...', function(done)
	//~ {
		//~ db.ref('commands/stop').set(1);
	//~ });

});