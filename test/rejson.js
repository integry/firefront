var assert = require('assert');
const _ = require('lodash');
const firefront = require('../firefront.js');

var chai = require('chai');
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style

firefront.initializeApp('test-redis');

var db = firefront.database('test-redis');

var key = ('test' + Math.random().toString()).split('.').join('');

describe('Setting and receiving data', function() {

	var value = 'blah blah';

	it('should send and receive the same data', function(done)
	{
		db.ref(key).on('value', function(snap)
		{
			if (!snap.val())
			{
				return;
			}

			assert.equal(value, snap.val());
			done();
		});

		// @todo: setting a watcher and a value at the same time creates race condition and it's possible that the value is never sent
		setTimeout(function()
		{
			db.ref(key).set(value);
		});
	});

	it('set and read deep values', function(done) {

		var root = db.ref(key + 'DEEP');

		root.child('very').child('deep/value').on('value', function(snap)
		{
			if (!snap.val())
			{
				return;
			}

			assert.equal(value + value, snap.val());
			done();
		});

		root.child('very/deep/value').set(value + value);
	});

	it('parent should be notified on changes in deep values', function(done) {

		db.ref(key + 'deep-parent').child('very').on('value', function(snap)
		{
			if (!snap.val())
			{
				return;
			}

			assert.equal(JSON.stringify({"super": {"deep": { "value": value + value}}}), JSON.stringify(snap.val()));
			done();
		});

		db.ref(key + 'deep-parent').child('very/super/deep/value').set(value + value);
	});

	it('should return keys only', function(done) {
		var ref = db.ref(key + 'test-keys');
		ref.child('a').set(1);
		ref.child('b').set(2);
		ref.child('c').set(3);

		ref.keys().on('value', function(snap)
		{
			if (!snap.val())
			{
				return;
			}

			if (snap.val().length == 3)
			{
				assert.equal(JSON.stringify(['a', 'b', 'c']), JSON.stringify(snap.val().sort()));
				done();
			}
		});
	});

	it('should return root keys only', function(done) {

		var root = db.ref();
		root.keys().on('value', function(snap)
		{
			var keys = snap.val();

			expect(keys.length).to.be.gt(0);
			expect(keys.indexOf(key + 'deep-parent')).to.be.gt(-1);

			done();
		});
	});

	it.only('should return empty list of keys for the deepest node', function(done) {
		var ref = db.ref(key + 'deep-node-keys');
		ref.child('a').set(1, function(snap)
		{
			ref.child('a').keys().on('value', function(snap)
			{
				console.log('deeeeeeeeeeeeeeeep keys', snap.val());
				if (!snap.val())
				{
					return;
				}

				if (snap.val().length == 3)
				{
					assert.equal(JSON.stringify(['a', 'b', 'c']), JSON.stringify(snap.val().sort()));
					done();
				}
			});
		});
	});

	it('should return shallow values', function(done) {

		var ref = db.ref(key + 'shallow');
		ref.child('a').set({test: true});
		ref.child('b').set(2);
		ref.child('c').set({a: 1, b: {a: 1, c: 3}});

		ref.shallow().on('value', function(snap)
		{
			var val = snap.val();
			if (!_.keys(val).length)
			{
				return;
			}

			assert.equal(JSON.stringify(val.a), '{}');
			assert.equal(JSON.stringify(val.b), 2);
			assert.equal(JSON.stringify(val.c), '{}');
			done();
		});
	});
});