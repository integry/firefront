var assert = require('assert');
const _ = require('lodash');
const firefront = require('../firefront.js');

firefront.initializeApp('test');

var db = firefront.database('test');

var key = 'test' + Math.random().toString();

// set notification
describe('Setting and receiving data', function() {

	var value = 'blah blah';

	it('should send and receive the same data', function(done) {

		// @todo: fails without the timeout
		setTimeout(function() {
			db.ref(key).on('value', function(snap)
			{
				console.log('Received', snap.val());
				assert.equal(value, snap.val());
				console.log('done?');
				done();
				console.log('done!');
			});

			db.ref(key).set(value);
		});
	});

	it('should send and receive the same data #2', function(done) {
		db.ref(key + '2').on('value', function(snap)
		{
			console.log('Received #2', snap.val());
			assert.equal(value + value, snap.val());
			done();
		});

		db.ref(key + '2').set(value + value);
	});

	it('should receive previously set value', function(done) {

		db.ref(key).on('value', function(snap)
		{
			console.log('Received #3', snap.val());
			assert.equal(value, snap.val());
			done();
		});
	});
});

// write callback
describe('Set callback', function() {

	it('should callback after a successful write', function(done) {
		var updateKey = key + '-setcallback';

		db.ref(updateKey).set('abc', function()
		{
			db.ref(updateKey).once('value', function(snap)
			{
				assert.equal('abc', snap.val());
				done();
			});
		});
	});
});


// on - multiple notifications on every change
describe('Receiving multiple data updates', function() {

	it('value should be increased sequentially', function(done) {
		var value = 0;
		db.ref(key + '-multi').on('value', function(snap)
		{
			assert.equal(++value, snap.val());

			if (3 == value)
			{
				done();
			}
		});

		var c = 0;
		var i = setInterval(function()
		{
			db.ref(key + '-multi').set(++c);

			if (3 == c)
			{
				clearInterval(i);
			}
		}, 100);
	});
});

// on / once
describe('Notify only once', function() {

	it('callback should be fired only once', function(done) {
		db.ref(key + '-once').once('value', function(snap)
		{
			assert.equal(1, snap.val());
			done();
		});

		var c = 0;
		var i = setInterval(function()
		{
			db.ref(key + '-once').set(++c);

			if (3 == c)
			{
				clearInterval(i);
			}
		}, 100);
	});
});

describe('Two "once" watchers', function() {

	it('should return the first value to both watchers', function(done) {
		db.ref(key + '-once-first').once('value', function(snap)
		{
			assert.equal(1, snap.val());
		});

		db.ref(key + '-once-first').once('value', function(snap)
		{
			assert.equal(1, snap.val());
			done();
		});

		var c = 0;
		var i = setInterval(function()
		{
			db.ref(key + '-once-first').set(++c);

			if (3 == c)
			{
				clearInterval(i);
			}
		}, 100);
	});
});

// child_added
describe('Append children to array', function() {

	function fillParent(key, speed)
	{
		speed = speed || 100;
		var c = 0;
		var i = setInterval(function()
		{
			db.ref(key).push(++c);

			if (3 == c)
			{
				clearInterval(i);
			}
		}, speed);
	}

	it('callbacks should be fired when new child nodes are added', function(done) {
		var value = 0;

		// only once
		db.ref(key + '-child_added').once('child_added', function(snap)
		{
			assert.equal(1, snap.val());
		});

		// all 3 children
		db.ref(key + '-child_added').on('child_added', function(snap)
		{
			assert.equal(++value, snap.val());

			if (3 == value)
			{
				done();
			}
		});

		fillParent(key + '-child_added');
	});

	it('value event for the parent node should be called only after the child_added events end', function(done) {

		db.ref(key + '-child_added_end').on('value', function(snap)
		{
			var val = snap.val();
			assert.equal(3, _.keys(val).length);
			done();
		});

		fillParent(key + '-child_added_end', 0);
	});

	it('callbacks should be fired only once when new child nodes are added', function(done) {

		db.ref(key + '-child_added_once').once('child_added', function(snap)
		{
			assert.equal(1, snap.val());
			done();
		});

		fillParent(key + '-child_added_once');
	});

	// @todo: test sequential keys
});

// existing data
describe('Notifications on existing data', function() {

	it('path should already exist and notify immediately', function(done) {
		var updateKey = key + '-existing-path';

		db.ref(updateKey).set(7, function()
		{
			db.ref(updateKey).once('value', function(snap)
			{
				assert.equal(7, snap.val());
				done();
			});
		});
	});

	it('existing child node should notify immediately', function(done) {
		var updateKey = key + '-existing';

		db.ref(updateKey).set({a: 1, b: 2, c: 3, d: 4}, function()
		{
			db.ref(updateKey).child('b').once('value', function(snap)
			{
				assert.equal(2, snap.val());
				done();
			});
		});
	});
});

// update (merge data)
describe('Merge data / partial update', function() {

	it('should notify parent when a child changes', function(done) {
		var updateKey = key + '-update';

		db.ref(updateKey).on('value', function(snap)
		{
			var val = snap.val();
			assert.equal(2, val.b);

			if (val.c == 'test')
			{
				done();
			}
		});

		db.ref(updateKey).set({a: 1, b: 2, c: 3, d: 4}, function()
		{
			setTimeout(function()
			{
				db.ref(updateKey).child('c').set('test');
			}, 1000);
		});
	});
});

// data persistence
describe('Data persistence', function() {

	it('should keep the same data after a server restart', function(done) {
		var updateKey = key + '-persistence';

		db.ref(updateKey).set('restarted', function()
		{
			db.cmd('restart');

			db.ref(updateKey).on('value', function(snap)
			{
				assert.equal(snap.val(), 'restarted');
				done();
			});
		});
	});
});

// 0 values

// unwatch path (stop receiving data notifications)

// child_changed

// child_removed

// do not send notifications when there are no watchers

// connection error (server gone)

// reconnect

// write onComplete error

// concurrent connections

// logging

// response compressing strategies