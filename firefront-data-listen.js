/* Push data straight from the client, instead of going through Firebase */

var ffServer = 'ws://localhost:2017/';

firefront = function()
{
	var connections = {};

	var p = function(path)
	{
		path = path || '';
		while (path.length && (path[0] == '/'))
		{
			path = path.substr(1, path.length);
		}

		while (path.length && (path[path.length] == '/'))
		{
			path = path.substr(0, path.length - 1);
		}

		return path;
	};

	var eventTypes = {
		child_added: '+',
		child_removed: '-',
		child_changed: '~',
		value: '='
	};

	var connect = function(database)
	{
		var socket = new WebSocket(ffServer + database);
		connections[database] = new function(socket)
		{
			var c = this;
			var watches = {};

			// @todo: queue messages, reconnect on error
			this.watch = function(path, type, callback)
			{
				path = p(path);

				if (!watches[path])
				{
					watches[path] = {};
				}

				wp = watches[path];

				if (!wp[type])
				{
					wp[type] = [];
					socket.send('+' + eventTypes[type] + path);
				}

				wp[type].push(callback);
			};

			this.unwatch = function(path, type, callback)
			{
				path = p(path);

				wp = watches[path];
				if (!wp || !wp[type])
				{
					return;
				}

				if (!callback)
				{
					wp[type] = [];
				}
				else
				{
					var i;
					while ((i = wp[type].indexOf(callback)) > -1)
					{
						wp[type].splice(i, 1);
					}
				}

				// @todo: assign IDs to paths server side
				if (!wp[type].length)
				{
					socket.send('-' + path);
				}
			};

			// @todo: snapshot key
			this.notify = function(path, type, value)
			{
				path = p(path);

				wp = watches[path];
				if (!wp || !wp[type])
				{
					this.unwatch(path, type);
					return;
				}

				var wt = wp[type];
				for (var k = 0; k < wt.length; k++)
				{
					var snapshot = {
						val: function()
						{
							return JSON.parse(JSON.stringify(value));
						},

						key: ''
					};

					wt[k](snapshot);
				}
			};

			return this;
		}(socket);

		// @todo: optimize data format
		socket.onmessage = function(event)
		{
			//~ var data = JSON.parse(event.data);
			//~ connections[database].notify(data.path, data.type, data.value);
			try
			{
				var data = JSON.parse(event.data);
			}
			catch (e) { console.log(event.data); }
			//~ console.log(data[1]);

			connections[database].notify(data[1], data[0], data[2]);
		};

		console.log(connections);
	};

	return {
		initializeApp: function(id)
		{
			connect(id);
		},

		database: function(id)
		{
			// @todo: use default id
			if (!id)
			{

			}

			var db = this;
			var c = connections[id];

			this.ref = function(path)
			{
				path = p(path);

				console.log(path);

				this.on = function(type, callback)
				{
					c.watch(path, type, callback);
				};

				// @todo: also tell server that it's once
				this.once = function(type, callback)
				{
					var cb = function(val)
					{
						callback(val);
						connections[id].unwatch(path, type, this);
					};
					cb.isOnce = true;

					c.watch(path, type, cb);
				};

				this.off = function(type, callback)
				{
					c.unwatch(path, type, callback);
				};

				this.child = function(subpath)
				{
					return new db.ref(path + '/' + subpath);
				};

				this.ref = this.child;
			}

			return new this.ref('');
		},

		turnOff: function()
		{

		},

		turnOn: function()
		{

		},
	}

}();
