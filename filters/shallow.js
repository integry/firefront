const _ = require('lodash');

module.exports = {
	filter: function(data, condition)
	{
		var ret = {};
		_.each(data, function(value, key)
		{
			ret[key] = _.isObjectLike(value) ? {} : value;
		});

		return ret;
	},
};
